-- --------------------------------------------------------
-- Hôte :                        localhost
-- Version du serveur:           5.7.19 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour finalsymfony
CREATE DATABASE IF NOT EXISTS `finalsymfony` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `finalsymfony`;

-- Listage de la structure de la table finalsymfony. article
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `categorie` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `publier` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23A0E66A76ED395` (`user_id`),
  CONSTRAINT `FK_23A0E66A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table finalsymfony.article : ~5 rows (environ)
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` (`id`, `title`, `url`, `description`, `date`, `categorie`, `user_id`, `publier`) VALUES
	(16, 'article 55', 'https://www.youtube.com/embed/D6tC1pyrsTM', 'sgchdsfdv<xwc', '2019-01-27 12:40:30', 'test', 4, 1),
	(17, 'article', 'https://www.youtube.com/embed/D6tC1pyrsTM', 'dsfgdfwsxb', '2019-01-27 12:46:22', 'test', 4, 1),
	(18, 'article 66', 'https://www.youtube.com/embed/D6tC1pyrsTM', 'ccxvcx', '2019-01-27 12:48:15', 'cbxvc', 4, 1),
	(19, 'article 77', 'https://www.youtube.com/embed/D6tC1pyrsTM', 'wxccbxcvb', '2019-01-27 12:51:07', 'test', 4, 0),
	(20, 'article 79', 'https://www.youtube.com/embed/D6tC1pyrsTM', 'xvcvcnbvcn', '2019-01-27 16:33:31', 'test', 3, 1);
/*!40000 ALTER TABLE `article` ENABLE KEYS */;

-- Listage de la structure de la table finalsymfony. migration_versions
CREATE TABLE IF NOT EXISTS `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table finalsymfony.migration_versions : ~0 rows (environ)
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;

-- Listage de la structure de la table finalsymfony. user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Listage des données de la table finalsymfony.user : ~2 rows (environ)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `email`, `lastname`, `firstname`, `password`, `birthday`, `username`) VALUES
	(3, 'user@gmail.com', 'user', 'user', '$2y$13$muKQfdG3zM.WuQVWDKOLZORSOLUjpY01zhIBYNhcuo/XT2CBt4fYK', '1916-07-18', 'user'),
	(4, 'admin@gmail.com', 'admin', 'admin', '$2y$13$uSG2zbpVA1cCxW8guxo8UukJ4Q7oXFCl41oiqIGqIHCufZQkPdapS', '1899-01-01', 'admin');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
