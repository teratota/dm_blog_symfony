<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use App\Form\RegisterType;

class UserController extends AbstractController
{
    /**
     * @Route("/inscription", name="user_inscription")
     */
    public function register(Request $requete, ObjectManager $manager, UserPasswordEncoderInterface $encode) //fonction pour inscrire un utilisateur
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($requete);
        if($form->isSubmitted() && $form->isValid()){
            $hash = $encode->encodePassword($user, $user->getPassword());
            $user->setPassword($hash);
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute("user_login");  
        }
        return $this->render('user/register.html.twig',[
            'form' => $form->createView()
        ]);
    }
   /**
    *@Route("/connexion", name="user_login")
    */
    public function login() //fonction pour connecter l'utilisateur ou l'administrateur
    {
	    return $this->render('user/login.html.twig');
    }
    
   /**
    *@Route("/deconnexion", name="user_logout")
    */
    public function logout() {} //fonction pour deconnecter l'utilisateur ou l'administrateur
    
}
