<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Article;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Form\ArticleType;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     */
    public function index() //fonction pour afficher les articles
    {
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('publier'=>1));
        $articles=$repo;
     
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/", name="home")
     */
    public function home() //fonction pour afficher la page d'acceuil du site
    {
        return $this->render('blog/home.html.twig');
    }

     /**
     * @Route("/blog/manage", name="manage")
     */
    public function manage() //fonction pour pour que l'admin puisse gerer les articles
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles=$repo->findAll();
        $admin="admin@gmail.com";
        $user = $this->get('security.token_storage')->getToken()->getUser();
        dump($articles);
        return $this->render('blog/manage.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles,
            'admin' => $admin,
            'user'  => $user
        ]);
    }

     /**
     * @Route("/blog/mymanage", name="my_article_manage")
     */
    public function mymanage() //fonction pour gerer les articles du user
    {
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $this->getUser(),'publier'=> 1));
        if(!$repo==NULL){
            dump($repo);
            $articles_publier=$repo;
        }else{
            $articles=NULL;
        }
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $this->getUser(),'publier'=> 0));
        if(!$repo==NULL){
            dump($repo);
            $articles_nonpublier=$repo;
        }else{
            $articles_nonpublier=NULL;
        }
        return $this->render('blog/mymanage.html.twig', [
            'controller_name' => 'BlogController',
            'articles_publier' => $articles_publier,
            'articles_nonpublier' => $articles_nonpublier,
        ]);
    }

    /**
     * @Route("/blog/listearticleuserpublier/{id}", name="liste_article_user_publier")
     */
    public function listearticleuserpublier($id) //fonction pour afficher la liste des article publier par un user
    {
        dump($id);
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $id,'publier'=> 1));
        $articles=$repo;
        return $this->render('blog/listearticleuserpublier.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/blog/ManageAdminListeArticleUser/{id}", name="blog_manage_admin_liste_article_user")
     */
    public function ManageAdminListeArticleUser($id) //fonction pour afficher la liste des article publier par un user pour l'admin
    {
        dump($id);
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $id,'publier'=> 1));
        if(!$repo==NULL){
            dump($repo);
            $articles_publier=$repo;
        }else{
            $articles_publier=NULL;
        }
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $this->getUser(),'publier'=> 0));
        if(!$repo==NULL){
            dump($repo);
            $articles_nonpublier=$repo;
        }else{
            $articles_nonpublier=NULL;
        }
        $admin="admin@gmail.com";
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('blog/ManageAdminListeArticleUser.html.twig', [
            'controller_name' => 'BlogController',
            'articles_publier' => $articles_publier,
            'articles_nonpublier' => $articles_nonpublier,
            'admin' => $admin,
            'user'  => $user
        ]);
    }

    /**
     * @Route("/blog/new", name="blog_create")
     * @Route("/blog/{id}/edit", name="blog_edit")
     */
    public function form(Article $article = null, Request $requete, ObjectManager $manager) //fonction pour gerer la création et la modification des articles
    {
        if(!$article){
            $article = new Article();
        }

        $form = $this->createForm(ArticleType::class, $article);
        
        $form->handleRequest($requete);
        if($form->isSubmitted() && $form->isValid()){
            if(!$article->getId()){
                $article->setdate(new \DateTime());
                $article->setuser($this->getUser());
            }
            $manager->persist($article);
            $manager->flush();

            return $this->redirectToRoute('blog_show', ['id' => $article->getId()]);
        }
        return $this->render('blog/create.html.twig', [
            'form_article' => $form->createView(),
            'edit' => $article->getId() !== null
        ]);
    }

    /**
     * @Route("/blog/{id}", name="blog_show")
     */
    public function show($id) //fonction pour afficher l'article avec son contenue
    {
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles=$repo->find($id);
        return $this->render('blog/show.html.twig', [
            'articles' => $articles
        ]);
    }

    /**
     * @Route("/deleteadmin/{id}", name="blog_delete_admin")
     */
    public function deleteadmin(Article $article) //fonction pour delete un article pour la fonction admin
    {
        $em = $this->getDoctrine()->getManager();
        $em ->remove($article);
        $em->flush();
        $repo = $this->getDoctrine()->getRepository(Article::class);
        $articles=$repo->findAll();
        $admin="admin@gmail.com";
        $user = $this->get('security.token_storage')->getToken()->getUser();
        return $this->render('blog/manage.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles,
            'admin' => $admin,
            'user'  => $user
        ]);
    }

    /**
     * @Route("/deleteuser/{id}", name="blog_delete_user")
     */
    public function deleteuser(Article $article) //fonction pour delete un article pour le user
    {
        $em = $this->getDoctrine()->getManager();
        $em ->remove($article);
        $em->flush();
        $repo = $this->getDoctrine()->getRepository(Article::class)->findBy(array('user'=> $this->getUser()));
        if(!$repo==NULL){
            $articles=$repo;
        }else{
            $articles=NULL;
        }
        return $this->render('blog/mymanage.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $articles,
        ]);
    }
    
   
    

}
