<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Article;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for($i = 1; $i <= 10; $i++){
            $article = new Article();
            $article->setTitle("article ".$i)
                ->seturl("https://www.youtube.com/watch?v=D6tC1pyrsTM")
                ->setdescription("contenue")
                ->setcategorie("test")
                ->setdate(new \DateTime());
            $manager->persist($article);
        }

        $manager->flush();
    }
}
